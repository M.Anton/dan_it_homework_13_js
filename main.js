const images = document.querySelectorAll(".image-to-show");
const stopButton = document.querySelector("#stop-button");
const resumeButton = document.querySelector("#resume-button");

let currentIndex = 0;
let intervalId = null;

function showImage() {
  for (let i = 0; i < images.length; i++) {
    if (i === currentIndex) {
      images[i].style.display = "block";
    } else {
      images[i].style.display = "none";
    }
  }
  currentIndex = (currentIndex + 1) % images.length;
}

function startSlideshow() {
  intervalId = setInterval(showImage, 3000);
}

function stopSlideshow() {
  clearInterval(intervalId);
}

stopButton.addEventListener("click", stopSlideshow);

resumeButton.addEventListener("click", () => {
  stopSlideshow();
  showImage();
  startSlideshow();
});

showImage();
startSlideshow();
